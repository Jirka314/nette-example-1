Nette example 1
=================


Requirements
------------

 [Docker](https://docs.docker.com/engine/), [docker-compose](https://docs.docker.com/compose/)


Installation
------------
1) open project directory in terminal
2) ``docker-compose up -d``
3) install composer (``docker exec -it nette-example_1_composer_1 bash `` ``composer install``)
4) ``docker exec -it nette-example_1_php-fpm_1 bash``
5) ``cd /application/``
6) ``chmod -R 777 temp``
7) ``chmod -R 777 log/``
8) open app http://localhost/ 



Run tests
----------
1) ``docker exec -it nette-example_1_php-fpm_1 bash``
2) ``cd /application/``
3) ``php vendor/bin/tester tests/``

Run phpStan
-----------
1) ``docker exec -it nette-example_1_php-fpm_1 bash``
2) ``cd /application/``
3) ``vendor/bin/phpstan analyse app``

Run code shifter
----------------
1) ``docker exec -it nette-example_1_php-fpm_1 bash``
2) ``cd /application/``
3) `` vendor/bin/phpcs --standard=ruleset.xml app tests ``

Run code fixer
----------------
1) ``docker exec -it nette-example_1_php-fpm_1 bash``
2) ``cd /application/``
3) ``vendor/bin/phpcbf --standard=ruleset.xml app tests``