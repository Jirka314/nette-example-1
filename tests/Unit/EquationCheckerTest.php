<?php declare(strict_types = 1);

namespace Tests\Unit;

use App\Services\EquationChecker;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../../vendor/autoload.php';
class EquationCheckerTest extends TestCase
{

	public function testSame(): void
	{
		$checker = new EquationChecker();
		$result = $checker->checkEquation('-5 + 1 = -4');
		Assert::true($result);
		$result = $checker->checkEquation('-5 = 3 + 8 - 10 - 6');
		Assert::true($result);
		$result = $checker->checkEquation('7 + 4 = 3 + 1 +5 + 2');
		Assert::true($result);
	}

	public function testDifferent(): void
	{
		$checker = new EquationChecker();
		$result = $checker->checkEquation('-3 + 1 = -4');
		Assert::false($result);
		$result = $checker->checkEquation('-8 = 3 + 8 - 10 - 6');
		Assert::false($result);
		$result = $checker->checkEquation('7 + 4 = 3 + 1 +5 + 3');
		Assert::false($result);
	}

}

(new EquationCheckerTest())->run();
