<?php declare(strict_types = 1);

namespace Tests\Unit;

use App\Services\InitialComparator;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../../vendor/autoload.php';
class InitialComparatorTest extends TestCase
{

	public function testSame(): void
	{
		$initialComparator = new InitialComparator();
		$result = $initialComparator->compare('Jan Jirásek');
		Assert::true($result);
	}

	public function testDifferent(): void
	{
		$initialComparator = new InitialComparator();
		$result = $initialComparator->compare('Jan Korejtko');
		Assert::false($result);
	}

}

(new InitialComparatorTest())->run();
