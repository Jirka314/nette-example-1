<?php declare(strict_types = 1);

use Tester\Environment;

require __DIR__ . '/../vendor/autoload.php'; // načte Composer autoloader

Environment::setup();
