'use strict'

document.addEventListener('DOMContentLoaded', function() {
    const links = document.getElementsByClassName('confirm');
    for (let i = 0, len = links.length; i < len; i++) {
        links[i].addEventListener('click', function (event) {
            const action = confirm(links[i].dataset.confirm) ? true : event.preventDefault();
        });
    }
});