<?php declare(strict_types = 1);

namespace App;

use Nette\Bootstrap\Configurator;

class Bootstrap
{

	public static function boot(): Configurator
	{
		date_default_timezone_set('Europe/Prague');
		$configurator = new Configurator();
		$appDir = dirname(__DIR__);

		$configurator->setDebugMode('10.5.0.1'); // enable for docker network gateway
		$configurator->enableTracy($appDir . '/log');

		$configurator->setTempDirectory($appDir . '/temp');

		$configurator->createRobotLoader()
			->addDirectory(__DIR__)
			->register();

		$configurator->addConfig($appDir . '/config/common.neon');
		$configurator->addConfig($appDir . '/config/services.neon');

		return $configurator;
	}

}
