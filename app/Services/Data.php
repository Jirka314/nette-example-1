<?php declare(strict_types = 1);

namespace App\Services;

use App\Entity\Row;
use Nette\Utils\DateTime;

/**
 * loads and parses data from a remote source
 */
class Data
{

	public function __construct(private readonly string $dataUrl)
	{
	}

	/**
	 * @return Row[]
	 * @throws DataUnavailable
	 * @throws InvalidData
	 */
	public function getAll(): array
	{
		$dataString = file_get_contents($this->dataUrl);
		if ($dataString === false) {
			throw new DataUnavailable();
		}

		try {
			$objectArray = json_decode($dataString, associative: true, flags: JSON_THROW_ON_ERROR);
		} catch (\Throwable $e) {
			throw new InvalidData();
		}

		return array_map([$this, 'mapRow'], $objectArray);
	}

	/**
	 * @param string[] $value
	 * @throws \Exception
	 */
	private function mapRow(array $value): Row
	{
		$timestamp = strtotime($value['createdAt']);
		if ($timestamp === false) {
			throw new InvalidData();
		}

		try {
			return new Row(
				(int) $value['id'],
				$value['name'],
				floatval($value['firstNumber']),
				floatval($value['secondNumber']),
				floatval($value['thirdNumber']),
				$value['calculation'],
				$value['joke'],
				DateTime::from($timestamp),
			);
		} catch (\Throwable $e) {
			throw new InvalidData();
		}
	}

}
