<?php declare(strict_types = 1);

namespace App\Services;

use Nette\Utils\Arrays;
use Nette\Utils\Strings;

/**
 * Compares initials of first and last name, ignoring all other parts of the name
 */
class InitialComparator
{

	public function compare(string $name): bool
	{
		$initials = $this->getInitials($name);

		return Arrays::first($initials) === Arrays::last($initials);
	}

	/**
	 * @return string[]
	 */
	private function getInitials(string $name): array
	{
		return array_map(fn (string $namePart) => Strings::substring($namePart, 0, 1), explode(' ', $name));
	}

}
