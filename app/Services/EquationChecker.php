<?php declare(strict_types = 1);

namespace App\Services;

/**
 * Calculate and compare the right and left sides of a simple equality.
 * support operations: +, -
 * Brackets are not supported
 */
class EquationChecker
{

	public function checkEquation(string $equation): bool
	{
		[$left, $right] = explode('=', $equation);

		return $this->eval($left) === $this->eval($right);
	}

	private function eval(string $expression): int
	{
		$result = 0;
		$actualOperation = '+';
		$token = strtok($expression, ' ');
		while ($token !== false) {
			if ($token === '+') {
				$actualOperation = '+';
			} elseif ($token === '-') {
				$actualOperation = '-';
			} else {
				if ($actualOperation === '+') {
					$result += intval($token);
				} else {
					$result -= intval($token);
				}
			}

			$token = strtok(' ');
		}

		return $result;
	}

}
