<?php declare(strict_types = 1);

namespace App\UI\Component;

interface TableFactory
{

	public function create(): Table;

}
