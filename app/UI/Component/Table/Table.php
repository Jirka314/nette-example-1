<?php declare(strict_types = 1);

namespace App\UI\Component;

use App\Entity\Row;
use Nette\Application\UI\Control;
use Nette\Bridges\ApplicationLatte\DefaultTemplate;

/**
 * @property-read DefaultTemplate $template
 */
class Table extends Control
{

	/**
	 * @param Row[] $rows
	 */
	public function render(array $rows): void
	{
		$this->template->rows = $rows;
		$this->template->render(__DIR__ . '/table.latte');
	}

}
