<?php declare(strict_types = 1);

namespace App\UI\Home;

use App\Entity\Row;
use App\Services\Data;
use App\Services\DataUnavailable;
use App\Services\EquationChecker;
use App\Services\InitialComparator;
use App\Services\InvalidData;
use App\UI\Component\Table;
use App\UI\Component\TableFactory;
use Nette\Application\UI\Presenter;
use Nette\Bridges\ApplicationLatte\DefaultTemplate;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

/**
 * @property-read DefaultTemplate $template
 */
final class HomePresenter extends Presenter
{

	/** @inject */
	public Data $data;

	/** @inject */
	public InitialComparator $initialComparator;

	/** @inject */
	public TableFactory $tableFactory;

	/** @inject */
	public EquationChecker $equationChecker;

	/** @var Row[] */
	private array $rows = [];

	public function renderDefault(): void
	{
		$this->template->rows = $this->rows;
	}

	public function renderJoke(): void
	{
		$shortJokes = array_map(function (Row $row) {
			$index = 0;
			$result = ['', ''];
			foreach (explode(' ', $row->joke) as $word) {
				if ($index === 0 && strlen($result[0]) > strlen($row->joke) * 0.5) $index++;
				$result[$index] .= $word . ' ';
			}

			return $result;
		}, array_filter($this->rows, fn (Row $row) => Strings::length($row->joke) <= 120));
		$this->template->joke = $shortJokes[array_rand($shortJokes)];
	}

	public function renderInitials(): void
	{
		$this->template->rows = array_filter($this->rows, fn (Row $row) => $this->initialComparator->compare($row->name));
	}

	public function renderCalculation(): void
	{
		$this->template->rows = array_filter($this->rows, fn (Row $row) => $row->firstNumber / $row->secondNumber === $row->thirdNumber && $row->firstNumber % 2 === 0);
	}

	public function renderTimeInterval(): void
	{
		$this->template->rows = array_filter($this->rows, function (Row $row) {
			$intervalStart = (new DateTime())->sub(new \DateInterval('P1M'));
			$intervalEnd = (new DateTime())->add(new \DateInterval('P1M'));

			return $intervalStart->getTimestamp() <= $row->createAt->getTimestamp() && $intervalEnd->getTimestamp() >= $row->createAt->getTimestamp();
		});
	}

	public function renderParser(): void
	{
		$this->template->rows = array_filter($this->rows, fn (Row $row) => $this->equationChecker->checkEquation($row->calculation));
	}

	protected function startup(): void
	{
		try {
			$this->rows = $this->data->getAll();
		} catch (DataUnavailable $e) {
			$this->flashMessage('Data se nepodařilo načíst.');
		} catch (InvalidData $e) {
			$this->flashMessage('Data obsahují chybu.');
		}

		parent::startup();
	}

	protected function createComponentTable(): Table
	{
		return $this->tableFactory->create();
	}

}
