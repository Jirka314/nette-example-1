<?php declare(strict_types = 1);

namespace App\Entity;

use Nette\Utils\DateTime;

class Row
{

	public function __construct(
		public readonly int $id,
		public readonly string $name,
		public readonly float $firstNumber,
		public readonly float $secondNumber,
		public readonly float $thirdNumber,
		public readonly string $calculation,
		public readonly string $joke,
		public readonly DateTime $createAt
	)
	{
	}

}
